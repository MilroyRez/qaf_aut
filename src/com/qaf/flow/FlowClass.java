package com.qaf.flow;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qaf.pojo.CartDetails;
import com.qaf.pojo.Item;
import com.qaf.pojo.Scenario;
import com.qaf.utill.DriverFactory;
import com.qaf.utill.PropertySingleton;

public class FlowClass {
	
private Scenario CurrentScenario = null;
private WebDriver  driver        = null;
	public FlowClass(Scenario scenario){
		CurrentScenario = scenario;
		driver = DriverFactory.getInstance().getDriver();
	}
	//test
public void moveTologinMainPage(){
	DriverFactory.getInstance().getDriver().findElement(By.id("")).click();	
}
	
public void login(){
	driver.findElement(By.id("")).sendKeys("");
	driver.findElement(By.id("")).sendKeys("");
	driver.findElement(By.id("")).click();
}

public void selectNormalFilters(){
    ArrayList<String> NormalFilters = CurrentScenario.getNormalSizes();
    for (int i = 0; i < NormalFilters.size(); i++) {
		String Filter = NormalFilters.get(i).trim();
		
		if(i != 0){
			addAdditionalFliter();
		}
		new Select(driver.findElement(By.id("filterSizeHide_"+Integer.toString(i+1)))).selectByVisibleText(Filter.trim());
		new Select(driver.findElement(By.id("qty"+Integer.toString(i+1)))).selectByVisibleText(Integer.toString(CurrentScenario.getNormalQTY()));
	}
}
public void addCustomFilters(){
		
	driver.findElement(By.partialLinkText("Add a custom filter size.")).click();
}
public void selectCustomFilters(){
	
    ArrayList<String> CustomFilters = CurrentScenario.getCustomSizes();
    for (int i = 0; i < CustomFilters.size(); i++) {
		String Filter = CustomFilters.get(i).trim();
		
		if(i != 0){
			addAdditionalCustomFliter();  
		}
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div["+Integer.toString(i+1)+"]/div[1]/select[1]"))).selectByVisibleText(Filter.split("x")[0]);
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div["+Integer.toString(i+1)+"]/div[2]/select[1]"))).selectByVisibleText(Filter.split("x")[1]);
		new Select(driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[1]/div/div/div["+Integer.toString(i+1)+"]/div[3]/select[1]"))).selectByVisibleText(Filter.split("x")[2]);
		new Select(driver.findElement(By.id("qty"+Integer.toString(i+1)))).selectByVisibleText(Integer.toString(CurrentScenario.getCustomQTY()));
	}
    
    
    driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div[4]/div/div/div[1]/div[2]/div[1]/form/div[2]/div/div/div[3]/div/button")).click();
	
}

public void addAdditionalFliter(){
	driver.findElement(By.className("add-filter")).click();
	
	
}
public void addAdditionalCustomFliter(){
	driver.findElement(By.className("custom-add-filter")).click();
}
public void proceedToSubscription(){
	driver.findElement(By.name("continue")).click();
	new WebDriverWait(driver, 80).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='top']/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")));
}

public void getStarted(){
	driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div/div/div/section[1]/div[1]/div[2]/a")).click();
}
public void accessThroughRefferal(){
	
}

public void accessThroughDiscount(){
	
}

public String getBannerDiscountText(){
	return driver.findElement(By.xpath("//*[@id=\"top\"]/body/div[3]/div/div[4]/p")).getText();
}


public void selectSubscription() throws InterruptedException{
	Thread.sleep(5000);
	if(CurrentScenario.getSubscription().equalsIgnoreCase("One Time")){
		driver.findElement(By.xpath(".//*[@id='onetimeform']/button")).click();  
					
	}else if(CurrentScenario.getSubscription().equalsIgnoreCase("Two Months")){
		driver.findElement(By.xpath(".//*[@id='top']/body/div[3]/div/div[4]/div[1]/div[2]/div[2]/form/button")).click();		                             
		
	}else if(CurrentScenario.getSubscription().equalsIgnoreCase("Three Months")){
		driver.findElement(By.xpath(".//*[@id='top']/body/div[3]/div/div[4]/div[1]/div[2]/div[3]/form/button")).click();
		
	}else if(CurrentScenario.getSubscription().equalsIgnoreCase("Six Months")){
		driver.findElement(By.xpath(".//*[@id='top']/body/div[3]/div/div[4]/div[1]/div[2]/div[4]/form/button")).click();
		
	}
	
}

public String getAverageSubscriptionvalue(){
	if(CurrentScenario.getQuality().equalsIgnoreCase("Standard")){
		return driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[2]/form/div/div[1]/div/div/div/h6")).getText();
			
	}else if(CurrentScenario.getQuality().equalsIgnoreCase("Super")){
		return driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/div[2]/div/div/h6")).getText();
		
	}else if(CurrentScenario.getQuality().equalsIgnoreCase("Elite")){
		return driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[4]/form/div/div[1]/div/div/h6")).getText();
		
	}else {
		return "N/A";

	}
	
}

public void selectQality(){
	
	if(CurrentScenario.getQuality().equalsIgnoreCase("Standard")){
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[2]/form/div/button")).click();
			
	}else if(CurrentScenario.getQuality().equalsIgnoreCase("Super")){
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[3]/form/div/button")).click();
		
	}else if(CurrentScenario.getQuality().equalsIgnoreCase("Elite")){
		driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/section[2]/div/div/div/div[4]/form/div/button")).click();
		
	}
	
}

public void proceedToPayment(){
	
}

public CartDetails getCartDetails(){
	
	CartDetails cart = new CartDetails();
	int totalCont = CurrentScenario.getNormalTypes() + CurrentScenario.getCustomTypes();
	cart.setAverageCost(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/span/span")).getText());
	for (int i = 0; i < totalCont; i++) {
	  Item item = new Item();
	  item.setSIZE(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tbody[2]/tr["+Integer.toString(i+1)+"]/td[1]/h2")).getText());
	  item.setQTY(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tbody[2]/tr["+Integer.toString(i+1)+"]/td[2]/div/span")).getText());
	  item.setSUBTOTAL(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tbody[2]/tr["+Integer.toString(i+1)+"]/td[3]/span/span")).getText());
	  cart.addToItems(item);	
	}
	
	cart.setFrequency(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[2]/td")).getText());
	cart.setQuality(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[3]/td")).getText());
	cart.setSubtotal(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[4]/td[2]/span")).getText());
	cart.setDiscount(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[5]/td[2]/strong/span")).getText());
	cart.setTax(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[6]/td[2]/span")).getText());
	
	if(!CurrentScenario.getSubscription().equalsIgnoreCase("One Time")){
	cart.setShipping(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[7]/td[2]/span")).getText());
	cart.setGrandTotal(driver.findElement(By.xpath("/html/body/div[3]/div/div[6]/div[4]/div/div/ol/li[3]/form/div[2]/ol/div[1]/li/div[3]/div/table/tfoot/tr[8]/td[2]/strong/span")).getText());
	}else{
	cart.setShipping(driver.findElement(By.xpath("//*[@id='checkout-review-table']/tfoot/tr[6]/td[2]/span")).getText());
	cart.setGrandTotal(driver.findElement(By.xpath("//*[@id='checkout-review-table']/tfoot/tr[7]/td[2]/strong/span")).getText());
	}
	return cart;

}

public void fillBillingInfo(){
	
	driver.findElement(By.id("billing:firstname")).sendKeys("Roy");
	driver.findElement(By.id("billing:lastname")).sendKeys("Perera");
	driver.findElement(By.id("billing:street1")).sendKeys("1250 Capital of TX Hwy South, Bldg. 1 Ste 400");
    new Select(driver.findElement(By.id("billing:region_id"))).selectByVisibleText("Texas");
	driver.findElement(By.id("billing:postcode")).sendKeys("78746");
	driver.findElement(By.id("billing:city")).sendKeys("Austin");
	driver.findElement(By.id("billing:telephone")).sendKeys("0789834328");
	
	driver.findElement(By.id("billing:email")).sendKeys(CurrentScenario.getCCType());
	
	//driver.findElement(By.id("billing:email")).sendKeys("royoty@gmail.com");
	driver.findElement(By.id("billing:customer_password")).sendKeys("123456");
	driver.findElement(By.id("billing:confirm_password")).sendKeys("123456");
}

public void fillCCInfo() throws Exception{
	driver.findElement(By.id("cryozonic_stripe_cc_owner")).sendKeys(PropertySingleton.getInstance().getProperty("CC.Name"));
	driver.findElement(By.id("cryozonic_stripe_cc_number")).sendKeys(PropertySingleton.getInstance().getProperty("CC.Number"));
	driver.findElement(By.id("cryozonic_stripe_cc_cid")).sendKeys(PropertySingleton.getInstance().getProperty("CC.CVV"));
    new Select(driver.findElement(By.id("cryozonic_stripe_expiration"))).selectByVisibleText(PropertySingleton.getInstance().getProperty("CC.Month"));
    new Select(driver.findElement(By.id("cryozonic_stripe_expiration_yr"))).selectByVisibleText(PropertySingleton.getInstance().getProperty("CC.Year"));
    Thread.sleep(8000);
	driver.findElement(By.id("onestepcheckout-button-place-order")).click();
	Thread.sleep(9000);
}

public void selectCreditCard(){
	
}

public void clickSignIn(){
	
}
public void clickSignUp(){
	
}
public void SignUp(){
	
}

public String generateReferLink(){
	
    driver.get("http://client.qualityairfilters.com/");
    driver.findElement(By.xpath(".//*[@id='top']/body/div[3]/div/div[1]/div/div/div[1]/ul[1]/li[4]/a")).click(); 
    driver.findElement(By.id("email")).sendKeys("h.milroyperera@gmail.com");
    driver.findElement(By.id("pass")).sendKeys("123456");
    driver.findElement(By.id("send2")).click(); 
    driver.findElement(By.xpath(".//*[@id='top']/body/div[3]/div/div[1]/div/div/div[1]/ul[1]/li[1]/a")).click(); 
    driver.findElement(By.id("copypath")).click(); 
    
	return "value";
}
//test
public void clickRefferal(){
	
}

public void refferToAFiriend(){
	
}

public void checkout(){
	
}
}

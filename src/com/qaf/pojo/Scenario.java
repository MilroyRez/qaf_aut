package com.qaf.pojo;

import java.util.ArrayList;

public class Scenario {

	private String AccessType = null;
	private String UserType = null;
	private String FilterTypes = null;
	private int NormalTypes = 0;
	private ArrayList<String> NormalSizes = null;
	private int NormalQTY = 0;
	private int CustomTypes = 0;
	private ArrayList<String> CustomSizes = null;
	private int CustomQTY = 0;
	private String Subscription = null;
	private String Quality = null;
	private String CCType = null;
	//private String email =null;



	public String getAccessTypes() {
		return AccessType;
	}

	public void setAccessTypes(String accessTypes) {
		AccessType = accessTypes;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}

	public String getFilterTypes() {
		return FilterTypes;
	}

	public void setFilterTypes(String filterTypes) {
		FilterTypes = filterTypes;
	}

	public int getNormalTypes() {
		return NormalTypes;
	}

	public void setNormalTypes(int normalTypes) {
		NormalTypes = normalTypes;
	}

	public ArrayList<String> getNormalSizes() {
		return NormalSizes;
	}

	public void setNormalSizes(ArrayList<String> normalSizes) {
		NormalSizes = normalSizes;
	}

	public int getNormalQTY() {
		return NormalQTY;
	}

	public void setNormalQTY(int normalQTY) {
		NormalQTY = normalQTY;
	}

	public int getCustomTypes() {
		return CustomTypes;
	}

	public void setCustomTypes(int customTypes) {
		CustomTypes = customTypes;
	}

	public ArrayList<String> getCustomSizes() {
		return CustomSizes;
	}

	public void setCustomSizes(ArrayList<String> customSizes) {
		CustomSizes = customSizes;
	}

	public int getCustomQTY() {
		return CustomQTY;
	}

	public void setCustomQTY(int customQTY) {
		CustomQTY = customQTY;
	}

	public String getSubscription() {
		return Subscription;
	}

	public void setSubscription(String subscription) {
		Subscription = subscription;
	}

	public String getQuality() {
		return Quality;
	}

	public void setQuality(String quality) {
		Quality = quality;
	}

	public String getCCType() {
		return CCType;
	}

	public void setCCType(String cCType) {
		CCType = cCType;
	}
	
	//public String getEmail() {
	//	return email;
	//}

//	public void setEmail(String cemail) {
	//	this.email = cemail;
	//	}
	}



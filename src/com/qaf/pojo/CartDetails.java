package com.qaf.pojo;

import java.util.ArrayList;

public class CartDetails {

	private String AverageCost = null;
	private String Frequency = null;
    private ArrayList<Item> Items = null;
    private String Subtotal = null;
    private String Discount = null;
	private String Tax = null;
	private String Shipping = null;
	private String GrandTotal = null;
	private String Quality = null;
	
	public String getAverageCost() {
		return AverageCost;
	}
	public void setAverageCost(String averageCost) {
		AverageCost = averageCost;
	}
	public String getFrequency() {
		return Frequency;
	}
	public void setFrequency(String frequency) {
		Frequency = frequency;
	}
	public ArrayList<Item> getItems() {
		return Items;
	}
	public void setItems(ArrayList<Item> items) {
		Items = items;
	}
	
	public void addToItems(Item items) {
		if(Items == null){
			Items = new ArrayList<Item>();
		}
		Items.add(items);
	}
	public String getSubtotal() {
		return Subtotal;
	}
	public void setSubtotal(String subtotal) {
		Subtotal = subtotal;
	}
	public String getDiscount() {
		return Discount;
	}
	public void setDiscount(String discount) {
		Discount = discount;
	}
	public String getTax() {
		return Tax;
	}
	public void setTax(String tax) {
		Tax = tax;
	}
	public String getShipping() {
		return Shipping;
	}
	public void setShipping(String shipping) {
		Shipping = shipping;
	}
	public String getGrandTotal() {
		return GrandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		GrandTotal = grandTotal;
	}
	public String getQuality() {
		return Quality;
	}
	public void setQuality(String quality) {
		Quality = quality;
	}


}

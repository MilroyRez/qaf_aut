package com.qaf.pojo;

public class Item {

	private String SIZE  = null;
	private String QTY = null;
	private String SUBTOTAL = null;
	
	
	public String getSIZE() {
		return SIZE;
	}
	public void setSIZE(String sIZE) {
		SIZE = sIZE;
	}
	public String getQTY() {
		return QTY;
	}
	public void setQTY(String qTY) {
		QTY = qTY;
	}
	public String getSUBTOTAL() {
		return SUBTOTAL;
	}
	public void setSUBTOTAL(String sUBTOTAL) {
		SUBTOTAL = sUBTOTAL;
	}
	
	
	
}

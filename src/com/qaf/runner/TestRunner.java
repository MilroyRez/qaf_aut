package com.qaf.runner;
import com.qaf.utill.*;
import com.qaf.readers.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import com.qaf.flow.FlowClass;
import com.qaf.pojo.CartDetails;
import com.qaf.pojo.Scenario;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestRunner {

	private ExtentReports            report;
	private ExtentTest               logger;
	private String                   signupemail;
	private ArrayList<Scenario>      ScenarioList           = null ;
	private Map<Integer, String>     ScenarioMap            = null;
	private HashMap<String, String>  ProductDetails         = null;
	private String                   CalculatedRental       = null;
	HashMap<String, Boolean>         Pay_MandatoryList      = null;
	HashMap<String, String>          Pay_PlaceHolders       = null;
	HashMap<String, String>          Pay_ProductDetails     = null;
	HashMap<String, Boolean>         elements               = null;

	@Before
	public void setUp() throws Exception{

		ExcelReader reader = new ExcelReader();
		ScenarioMap =reader.init("C:/Users/Milroy/git/newgit/qaf/qaf/Resources/excels/Scenarios.xls").get(0);
		ScenarioList = new ScenarioDataLoader().loadWatchDetails(ScenarioMap);
	}


	@org.junit.Test
	public void runTest() throws Exception{


		Iterator<Scenario> iterator = ScenarioList.iterator();

		while (iterator.hasNext()) {
			Scenario   scenario = (Scenario) iterator.next();
			DriverFactory.getInstance().initializeDriver();
			FlowClass  Flow     =  new FlowClass(scenario); 


			if(scenario.getAccessTypes().equalsIgnoreCase("Direct")){
				DriverFactory.getInstance().getDriver().get(PropertySingleton.getInstance().getProperty("Base.Url"));
			}else if(scenario.getAccessTypes().equalsIgnoreCase("Discount")){
				DriverFactory.getInstance().getDriver().get(PropertySingleton.getInstance().getProperty("Discountlink"));
			}else if(scenario.getAccessTypes().equalsIgnoreCase("Referal")){
				DriverFactory.getInstance().getDriver().get(Flow.generateReferLink());
			}


			if(scenario.getUserType().equalsIgnoreCase("login")){
				Flow.login();
			}else if(scenario.getUserType().equalsIgnoreCase("Guest")){

			}

			    Flow.getStarted();

			if(scenario.getFilterTypes().equalsIgnoreCase("Both")){
				Flow.selectNormalFilters();
				Flow.addCustomFilters();
				Flow.selectCustomFilters();
			}else if(scenario.getAccessTypes().equalsIgnoreCase("NFliter")){
				Flow.selectNormalFilters();
			}else if(scenario.getAccessTypes().equalsIgnoreCase("Custom")){
				Flow.addCustomFilters();
				Flow.selectCustomFilters();
			}

			    Flow.proceedToSubscription();
			    Flow.selectSubscription();
			String SubValue = Flow.getAverageSubscriptionvalue();
			    Flow.selectQality();

			CartDetails cart = Flow.getCartDetails(); 

			if(scenario.getUserType().equalsIgnoreCase("Guest")){
				Flow.fillBillingInfo();
				Flow.fillCCInfo();
			}
		}
	}

	@After
	public void tearDown(){

	}
}

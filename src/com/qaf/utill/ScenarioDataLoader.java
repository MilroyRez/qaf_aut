package com.qaf.utill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import com.qaf.pojo.Scenario;

public class ScenarioDataLoader {

	public ScenarioDataLoader() {
		// dataLogger = org.apache.log4j.Logger.getLogger(this.getClass());
	}

	public ArrayList<Scenario> loadWatchDetails(Map<Integer, String> map) {
		ArrayList<Scenario> ReturnList = new ArrayList<Scenario>();

		Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();

		while (it.hasNext()) {
			Scenario CurrentScenario = new Scenario();
			String[] AllValues = it.next().getValue().split(",");

			System.out.println(AllValues[1]);

			CurrentScenario.setAccessTypes(AllValues[0]);
			CurrentScenario.setUserType(AllValues[1]);
			CurrentScenario.setFilterTypes(AllValues[2]);
			CurrentScenario.setNormalTypes(Integer.parseInt(AllValues[3].trim()));
			CurrentScenario.setNormalSizes(new ArrayList<String>(Arrays.asList(AllValues[4].trim().split("\\|"))));
			CurrentScenario.setNormalQTY(Integer.parseInt(AllValues[5].trim()));
			CurrentScenario.setCustomTypes(Integer.parseInt(AllValues[6].trim()));
			CurrentScenario.setCustomSizes(new ArrayList<String>(Arrays.asList(AllValues[7].trim().split("\\|"))));
			CurrentScenario.setCustomQTY(Integer.parseInt(AllValues[8].trim()));
			CurrentScenario.setSubscription(AllValues[9]);
			CurrentScenario.setQuality(AllValues[10]);
			CurrentScenario.setCCType(AllValues[11]);
		//	CurrentScenario.setEmail(AllValues[12]);
			
			ReturnList.add(CurrentScenario);
		}
		return ReturnList;

	}

}
